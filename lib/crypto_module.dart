
import 'package:crypto_module/interface/IFullKey.dart';

import 'crypto_module_platform_interface.dart';

class CryptoModule {

  Future<String?> getPlatformVersion() {
    return CryptoModulePlatform.instance.getPlatformVersion();
  }

  Future<IFullKey> generateKey(){
    return CryptoModulePlatform.instance.generateKey();
  }
}
