import 'dart:convert';

class IFullKey{
  String spendPublic;
  String spendPrivate;
  String viewPrivate;

  IFullKey({ required this.spendPrivate, required this.viewPrivate, required this.spendPublic});
  IFullKey.fromJson(Map<String, dynamic> json)
      : spendPublic = json['public_spend'],
        viewPrivate = json['private_view'],
        spendPrivate = json['private_spend'];

  String toJson() => jsonEncode({'public_spend': spendPublic,
      'private_view': viewPrivate,
      'private_spend': spendPrivate,})

  ;

}