import 'package:crypto_module/interface/IFullKey.dart';
import 'package:plugin_platform_interface/plugin_platform_interface.dart';

import 'crypto_module_method_channel.dart';

abstract class CryptoModulePlatform extends PlatformInterface {
  /// Constructs a CryptoModulePlatform.
  CryptoModulePlatform() : super(token: _token);

  static final Object _token = Object();

  static CryptoModulePlatform _instance = MethodChannelCryptoModule();

  /// The default instance of [CryptoModulePlatform] to use.
  ///
  /// Defaults to [MethodChannelCryptoModule].
  static CryptoModulePlatform get instance => _instance;
  
  /// Platform-specific implementations should set this with their own
  /// platform-specific class that extends [CryptoModulePlatform] when
  /// they register themselves.
  static set instance(CryptoModulePlatform instance) {
    PlatformInterface.verifyToken(instance, _token);
    _instance = instance;
  }

  Future<String?> getPlatformVersion() {
    throw UnimplementedError('platformVersion() has not been implemented.');
  }
  Future<IFullKey> generateKey() {
    throw UnimplementedError('platformVersion() has not been implemented.');
  }
}
