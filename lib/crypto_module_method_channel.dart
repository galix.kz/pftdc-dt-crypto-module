import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';

import 'crypto_module_platform_interface.dart';
import 'interface/IFullKey.dart';

/// An implementation of [CryptoModulePlatform] that uses method channels.
class MethodChannelCryptoModule extends CryptoModulePlatform {
  /// The method channel used to interact with the native platform.
  @visibleForTesting
  final methodChannel = const MethodChannel('crypto_module');

  @override
  Future<String?> getPlatformVersion() async {
    final version = await methodChannel.invokeMethod<String>('getPlatformVersion');
    return version;
  }

  @override
  Future<IFullKey> generateKey() async{
    String keys = await methodChannel.invokeMethod('generateKey');
    return IFullKey.fromJson(jsonDecode(keys));
  }
}
