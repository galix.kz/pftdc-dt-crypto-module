#import "CryptoModulePlugin.h"
#if __has_include(<crypto_module/crypto_module-Swift.h>)
#import <crypto_module/crypto_module-Swift.h>
#else
// Support project import fallback if the generated compatibility header
// is not copied when this plugin is created as a library.
// https://forums.swift.org/t/swift-static-libraries-dont-copy-generated-objective-c-header/19816
#import "crypto_module-Swift.h"
#endif

@implementation CryptoModulePlugin
+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
  [SwiftCryptoModulePlugin registerWithRegistrar:registrar];
}
@end
