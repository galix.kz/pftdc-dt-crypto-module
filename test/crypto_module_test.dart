import 'package:flutter_test/flutter_test.dart';
import 'package:crypto_module/crypto_module.dart';
import 'package:crypto_module/crypto_module_platform_interface.dart';
import 'package:crypto_module/crypto_module_method_channel.dart';
import 'package:plugin_platform_interface/plugin_platform_interface.dart';

class MockCryptoModulePlatform 
    with MockPlatformInterfaceMixin
    implements CryptoModulePlatform {

  @override
  Future<String?> getPlatformVersion() => Future.value('42');
}

void main() {
  final CryptoModulePlatform initialPlatform = CryptoModulePlatform.instance;

  test('$MethodChannelCryptoModule is the default instance', () {
    expect(initialPlatform, isInstanceOf<MethodChannelCryptoModule>());
  });

  test('getPlatformVersion', () async {
    CryptoModule cryptoModulePlugin = CryptoModule();
    MockCryptoModulePlatform fakePlatform = MockCryptoModulePlatform();
    CryptoModulePlatform.instance = fakePlatform;
  
    expect(await cryptoModulePlugin.getPlatformVersion(), '42');
  });
}
